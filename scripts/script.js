let newArray = [];

//Search field
const filteredArticle = async () => {
  var value = $("#search").val();
  let filter = []
  const posts = await mergedArray();
  let filterobj = {}
 posts.map(postitem => {
    if (postitem.title.includes(value)) {
      filterobj.title = postitem.title
      filterobj.id = postitem.id
      filterobj.body = postitem.body
      filterobj.thumbnailUrl = postitem.thumbnailUrl
      filter.push(filterobj)
    }
  });
  postComponent(filter);
};

$("#search").keyup(() => {
  filteredArticle()
})

//Fetching all Posts
const getAllPosts = async () => {
return $.ajax({
    type: "GET",
    url:'https://jsonplaceholder.typicode.com/posts',
    dataType: 'json',
    success: (data) => {
      return data
    }
  });
  
};

//Fetching all Photos
const getAllPhotos = async () => {
  const pic = $.ajax({
    type: 'GET',
    url: 'http://jsonplaceholder.typicode.com/photos',
    dataType: 'json',
    success: (data) => {
    }
  })
  return pic
};


//Mergin getAllPhotos and getAllPosts to a newArray
const mergedArray = async () => {
  newArray = [];
  const arrposts = await getAllPosts();
  const arrphotos = await getAllPhotos();
  arrposts.map(postitem => {
    var newobj = {};
     arrphotos.map(photoitem =>{
          if (photoitem.id === postitem.id){
            newobj.id = postitem.id
            newobj.title = postitem.title
            newobj.body = postitem.body
            newobj.url = photoitem.url
            newobj.thumbnailUrl = photoitem.thumbnailUrl
            newArray.push(newobj);
          }
      })
  })
  return newArray
};

// Creating the posts automaticaly
const postComponent = async (posts) => {
  $(document).ready(() => {
    $("#postsRow").html("")
  });
  posts.map(post => {
    if (post.body.length > 150){
      post.body = post.body.substring(0, 150) + "..."
    }
    $(document).ready(() => {
      $("#postsRow").append(`
        <div class="col-sm-4">
          <div class="card" id="card-home" style="width: 24rem; height: 39em;">
            <img src="${post.thumbnailUrl}" class="card-img-top" alt="...">
              <div class="card-body" >
                <h5 style="height: 60px"class="card-title" id="title" >${post.title}</h5>
                <p style="height: 90px" id="text" class="card-text">${post.body}</p>
                <a href="pages/post.html?id=${post.id}" class="btn btn-primary" id="${post.id}">${post.id}</a>
              </div>
          </div>
        </div>
        `)
    });
       
  });
}


//Main function
async function main() {
  const posts = await mergedArray();
  postComponent(posts);
}

main();

